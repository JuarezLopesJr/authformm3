package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.data.AuthMode
import com.example.authformm3.ui.screens.auth.AuthenticationTitle
import org.junit.Rule
import org.junit.Test

class AuthenticationTitleTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Sign_In_Title_Displayed() {
        composeTestRule.setContent { AuthenticationTitle(authMode = AuthMode.SIGN_IN) }
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(R.string.label_sign_in_to_account)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Sign_Up_Title_Displayed() {
        composeTestRule.setContent { AuthenticationTitle(authMode = AuthMode.SIGN_UP) }
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(R.string.label_sign_up_for_account)
        ).assertIsDisplayed()
    }
}