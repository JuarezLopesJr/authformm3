package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.data.AuthMode
import com.example.authformm3.utils.AuthButton
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATE_BUTTON
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class AuthenticationButtonTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Sign_In_Action_Displayed() {
        composeTestRule.setContent {
            AuthButton(authMode = AuthMode.SIGN_IN, enableAuth = true, onAuthClick = {})
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATE_BUTTON)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.action_sign_in)
            )
    }

    @Test
    fun assert_Sign_Up_Action_Displayed() {
        composeTestRule.setContent {
            AuthButton(authMode = AuthMode.SIGN_UP, enableAuth = true, onAuthClick = {})
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATE_BUTTON)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.action_sign_up)
            )
    }

    @Test
    fun assert_Authenticate_Triggered() {
        val onAuthenticate: () -> Unit = mock()

        composeTestRule.setContent {
            AuthButton(
                authMode = AuthMode.SIGN_UP,
                enableAuth = true,
                onAuthClick = onAuthenticate
            )
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATE_BUTTON).performClick()

        verify(onAuthenticate).invoke()
    }
}