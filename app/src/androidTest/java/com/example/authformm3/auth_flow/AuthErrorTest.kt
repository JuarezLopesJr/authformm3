@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalLifecycleComposeApi::class,
    ExperimentalMaterial3Api::class
)

package com.example.authformm3.auth_flow

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import com.example.authformm3.data.AuthState
import com.example.authformm3.ui.screens.auth.AuthenticationContent
import com.example.authformm3.ui.screens.auth.AuthenticationScreen
import com.example.authformm3.utils.Tags.TAG_ERROR_ALERT
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterial3Api
@ExperimentalLifecycleComposeApi class AuthErrorTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Error_Alert_Not_Displayed_By_Default(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent { AuthenticationScreen(navigator) }
        composeTestRule.onNodeWithTag(TAG_ERROR_ALERT).assertDoesNotExist()
    }

    @Test
    fun assert_Error_Alert_Is_Displayed_After_Error(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent {
            AuthenticationContent(
                authState = AuthState(error = "some error"),
                navigator = navigator,
                handleEvent = {}
            )
        }
        composeTestRule.onNodeWithTag(TAG_ERROR_ALERT).assertIsDisplayed()
    }
}