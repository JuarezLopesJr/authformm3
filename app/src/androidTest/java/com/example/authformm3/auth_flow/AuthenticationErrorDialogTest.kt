package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import com.example.authformm3.ui.screens.ErrorDialog
import com.example.authformm3.utils.Tags.TAG_ERROR_BUTTON
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class AuthenticationErrorDialogTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Error_Displayed() {
        val error = "This is an error"

        composeTestRule.setContent {
            ErrorDialog(error = error, dismissError = {})
        }

        composeTestRule.onNodeWithText(error).assertTextEquals(error)
    }

    @Test
    fun assert_Dismiss_Triggered_From_Action() {
        val error = "This is an error"
        val dismissError: () -> Unit = mock()

        composeTestRule.setContent {
            ErrorDialog(error = error, dismissError = dismissError)
        }

        composeTestRule.onNodeWithTag(TAG_ERROR_BUTTON).performClick()

        /* another way to reference the TextButton component
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(R.string.error_action)
        ).performClick()*/

        verify(dismissError).invoke()
    }
}