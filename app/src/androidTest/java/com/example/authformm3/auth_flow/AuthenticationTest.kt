package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsEnabled
import androidx.compose.ui.test.assertIsNotEnabled
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextClearance
import androidx.compose.ui.test.performTextInput
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.ui.screens.auth.AuthenticationScreen
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATE_BUTTON
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATION_TOGGLE
import com.example.authformm3.utils.Tags.TAG_INPUT_EMAIL
import com.example.authformm3.utils.Tags.TAG_INPUT_PASSWORD
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalLifecycleComposeApi
class AuthenticationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent { AuthenticationScreen(navigator) }
    }

    @Test
    fun assert_Sign_In_Title_Displayed_By_Default() {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry
                .getInstrumentation().targetContext.getString(R.string.label_sign_in_to_account)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Need_Account_Text_Displayed_By_Default() {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry
                .getInstrumentation().targetContext.getString(R.string.action_need_account)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Sign_Up_Title_Displayed_After_Toggled() {
        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(R.string.action_need_account)
        ).performClick()

        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(R.string.label_sign_up_for_account)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Sign_Up_Button_Displayed_After_Toggled() {
        composeTestRule.onNodeWithTag(TAG_AUTHENTICATION_TOGGLE).performClick()

        composeTestRule
            .onNodeWithTag(TAG_AUTHENTICATE_BUTTON)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.action_sign_up)
            )
    }

    @Test
    fun assert_Already_Have_Account_Displayed_After_Toggled() {
        composeTestRule.onNodeWithTag(TAG_AUTHENTICATION_TOGGLE).apply {
            performClick()
            assertTextEquals(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(
                        R.string.action_already_have_account
                    )
            )
        }
    }

    @Test
    fun assert_Auth_Button_Displayed_By_Default() {
        composeTestRule.onNodeWithTag(TAG_AUTHENTICATE_BUTTON).assertIsNotEnabled()
    }

    @Test
    fun assert_Auth_Button_Enabled_With_Valid_Content() {
        composeTestRule.apply {
            onNodeWithTag(TAG_INPUT_EMAIL).performTextInput("email@test.com")
            onNodeWithTag(TAG_INPUT_PASSWORD).performTextInput("password")
            onNodeWithTag(TAG_AUTHENTICATE_BUTTON).assertIsEnabled()
        }
    }

    @Test
    fun assert_Auth_Button_Disabled_From_Recomposition_With_Empty_Field() {
        composeTestRule.apply {
            onNodeWithTag(TAG_INPUT_EMAIL).performTextInput("email@test.com")

            onNodeWithTag(TAG_INPUT_PASSWORD).apply {
                performTextInput("password")
                performTextClearance()
            }

            onNodeWithTag(TAG_AUTHENTICATE_BUTTON).assertIsNotEnabled()
        }
    }
}