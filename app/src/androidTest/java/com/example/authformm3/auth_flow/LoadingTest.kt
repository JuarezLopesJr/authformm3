@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.auth_flow

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import com.example.authformm3.data.AuthState
import com.example.authformm3.ui.screens.auth.AuthenticationContent
import com.example.authformm3.ui.screens.auth.AuthenticationScreen
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATE_BUTTON
import com.example.authformm3.utils.Tags.TAG_PROGRESS
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import org.junit.Rule
import org.junit.Test

@ExperimentalMaterial3Api
@ExperimentalLifecycleComposeApi class LoadingTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Progress_Not_Displayed_By_Default(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent { AuthenticationScreen(navigator) }
        composeTestRule.onNodeWithTag(TAG_PROGRESS).assertDoesNotExist()
    }

    @Test
    fun assert_Progress_Displayed_While_Loading(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent {
            AuthenticationContent(
                authState = AuthState(isLoading = true),
                navigator = navigator,
                handleEvent = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_PROGRESS).assertIsDisplayed()
    }

    @Test
    fun assert_Progress_Not_Displayed_After_Loading(
        navigator: DestinationsNavigator = EmptyDestinationsNavigator
    ) {
        composeTestRule.setContent {
            AuthenticationContent(
                authState = AuthState(email = "email@test.com", password = "password"),
                navigator = navigator,
                handleEvent = {}
            )
        }
        composeTestRule.apply {
            onNodeWithTag(TAG_AUTHENTICATE_BUTTON).performClick()
            onNodeWithTag(TAG_PROGRESS).assertDoesNotExist()
        }
    }

    /* @Test
     fun assert_Content_Is_Displayed_After_Loading() {
         composeTestRule.setContent { AuthenticationScreen() }

         composeTestRule.apply {
             onNodeWithTag(TAG_INPUT_EMAIL).performTextInput("email@test.com")
             onNodeWithTag(TAG_INPUT_PASSWORD).performTextInput("password")
             onNodeWithTag(TAG_AUTHENTICATE_BUTTON).performClick()
             onNodeWithTag(TAG_CONTENT).assertExists()
         }
     }*/
}