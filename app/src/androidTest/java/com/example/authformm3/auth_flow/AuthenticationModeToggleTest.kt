package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.data.AuthMode
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATION_TOGGLE
import com.example.authformm3.utils.ToggleAuth
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class AuthenticationModeToggleTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Need_Account_Displayed() {
        composeTestRule.setContent {
            ToggleAuth(authMode = AuthMode.SIGN_IN, onToggleClick = {})
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATION_TOGGLE)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation().targetContext
                    .getString(R.string.action_need_account)
            )
    }

    @Test
    fun assert_Already_Have_Account_Displayed() {
        composeTestRule.setContent {
            ToggleAuth(authMode = AuthMode.SIGN_UP, onToggleClick = {})
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATION_TOGGLE)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation().targetContext
                    .getString(R.string.action_already_have_account)
            )
    }

    @Test
    fun assert_Toggle_Authentication_Triggered() {
        val onToggleAuth: () -> Unit = mock()

        composeTestRule.setContent {
            ToggleAuth(authMode = AuthMode.SIGN_UP, onToggleClick = onToggleAuth)
        }

        composeTestRule.onNodeWithTag(TAG_AUTHENTICATION_TOGGLE).performClick()

        verify(onToggleAuth).invoke()
    }
}