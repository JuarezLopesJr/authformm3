package com.example.authformm3.auth_flow

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.data.PasswordRequirements
import com.example.authformm3.utils.PasswordRequirements
import org.junit.Rule
import org.junit.Test

class PasswordInputTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val requirements = PasswordRequirements.values().toList()

    private val satisfiedRequirement =
        requirements[(0 until requirements.count()).random()]

    /*@Test
    fun assert_Password_Toggled_Reflects_State() {
        composeTestRule.setContent {
            PasswordInput(
                password = "password123",
                onPasswordChanged = {},
                onDoneClicked = {}
            )
        }
        composeTestRule.onNodeWithTag(TAG_PASSWORD_HIDDEN + "true").performClick()

        composeTestRule.onNodeWithTag(TAG_PASSWORD_HIDDEN + "false").assertIsDisplayed()
    }*/

    @Test
    fun assert_Password_Requirements_Displayed_As_Not_Satisfied() {
        composeTestRule.setContent {
            PasswordRequirements(requirements = listOf(satisfiedRequirement))
        }

        PasswordRequirements.values().forEach {
            val requirement = InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(it.label)

            val satisfied = InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(satisfiedRequirement.label)

            val result = if (requirement == satisfied) {
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.password_requirement_satisfied, requirement)
            } else {
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.password_requirement_needed, requirement)
            }

            composeTestRule.onNodeWithText(result).assertIsDisplayed()
        }
    }

    @Test
    fun assert_Password_Requirements_Displayed_With_State() {
        val satisfied = requirements[(0 until 3).random()]

        composeTestRule.setContent {
            PasswordRequirements(requirements = listOf(satisfied))
        }

        PasswordRequirements.values().forEach {
            val requirement = InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(it.label)

            val result = if (it == satisfied) {
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.password_requirement_satisfied, requirement)
            } else {
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(R.string.password_requirement_needed, requirement)
            }

            composeTestRule.onNodeWithText(result).assertIsDisplayed()
        }
    }
}