package com.example.authformm3.ui_flow

import android.content.res.Configuration
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.rememberNavController
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.utils.ContentArea
import com.example.authformm3.utils.SideBar
import com.example.authformm3.utils.Tags.TAG_FLOATING_BUTTON
import com.example.authformm3.utils.Tags.TAG_NAV_RAIL
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class NavRailTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            val configuration = LocalConfiguration.current
            val navController = rememberNavController()

            when (configuration.orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> {
                    SideBar(navController = navController)
                }
                else -> ContentArea(screen = BottomNavItem.Home)
            }
        }
    }

    @Test
    fun assert_Navigation_Rail_Displayed_In_Landscape() {
        composeTestRule.onNodeWithTag(TAG_NAV_RAIL).assertIsDisplayed()
    }

    @Test
    fun assert_Navigation_Rail_Not_Displayed_In_Landscape() {
        composeTestRule.onNodeWithTag(TAG_NAV_RAIL).assertDoesNotExist()
    }

    @Test
    fun assert_Navigation_Rail_Not_Displayed_For_Non_Root_Destination() {
        composeTestRule.onNodeWithTag(TAG_FLOATING_BUTTON).performClick()

        composeTestRule.onNodeWithTag(TAG_NAV_RAIL).assertDoesNotExist()
    }
}