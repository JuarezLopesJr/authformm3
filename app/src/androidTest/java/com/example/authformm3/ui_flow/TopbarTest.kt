package com.example.authformm3.ui_flow

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.ui.screens.feed.FeedScreen
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class TopbarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            val navController = rememberNavController()
            val navigator: DestinationsNavigator = EmptyDestinationsNavigator

            FeedScreen(navController = navController, navigator = navigator)
        }
    }

    @Test
    fun assert_Title_Displayed() {
        val title = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.feed_topbar_title)

        composeTestRule.onNodeWithText(title).assertIsDisplayed()
    }

    @Test
    fun assert_Icon_Displayed() {
        val title = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.cd_navback_information)

        composeTestRule.onNodeWithContentDescription(title).assertIsDisplayed()
    }

    @Test
    fun assert_Navigation_Icon_Triggers_Callback() {
        val onBackClicked: () -> Unit = mock()

        val title = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.cd_navback_information)

        composeTestRule.onNodeWithContentDescription(title).performClick()

        verify(onBackClicked).invoke()
    }
}