package com.example.authformm3.ui_flow

import androidx.compose.ui.test.assertContentDescriptionEquals
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.utils.ContentArea
import com.example.authformm3.utils.Tags.TAG_CONTENT_ICON
import com.example.authformm3.utils.Tags.TAG_CONTENT_TITLE
import org.junit.Rule
import org.junit.Test

class ContentAreaTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Destination_Displayed() {
        val destination = BottomNavItem.Feed

        composeTestRule.setContent { ContentArea(screen = destination) }

        composeTestRule.onNodeWithTag(
            InstrumentationRegistry.getInstrumentation().targetContext
                .getString(destination.label)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Content_Title_Displayed() {
        val destination = BottomNavItem.Calendar

        composeTestRule.setContent { ContentArea(screen = destination) }

        composeTestRule.onNodeWithTag(TAG_CONTENT_TITLE)
            .assertTextEquals(
                InstrumentationRegistry.getInstrumentation().targetContext
                    .getString(destination.label)
            )
    }

    @Test
    fun assert_Content_Icon_Displayed() {
        val destination = BottomNavItem.Contacts

        composeTestRule.setContent { ContentArea(screen = destination) }

        composeTestRule.onNodeWithTag(TAG_CONTENT_ICON)
            .assertContentDescriptionEquals(
                InstrumentationRegistry.getInstrumentation().targetContext
                    .getString(destination.label)
            )
    }
}