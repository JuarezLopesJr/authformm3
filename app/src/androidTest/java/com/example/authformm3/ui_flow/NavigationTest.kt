@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package com.example.authformm3.ui_flow

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.ui.screens.home.HomeScreen
import com.example.authformm3.utils.Tags.TAG_DESTINATION
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import com.ramcosta.composedestinations.navigation.navigate
import org.junit.Rule
import org.junit.Test

class NavigationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun assertNavigation(
        destination: BottomNavItem,
        navigator: DestinationsNavigator
    ) {
        composeTestRule.setContent {
            val navController = rememberNavController()

            HomeScreen(
                navController = navController,
                navigator = navigator
            )
            navController.navigate(destination.route)
        }

        composeTestRule.onNodeWithTag(
            TAG_DESTINATION + InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(destination.label), useUnmergedTree = true
        ).performClick()

        composeTestRule.onNodeWithTag(
            InstrumentationRegistry.getInstrumentation().targetContext
                .getString(destination.label)
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Home_Displayed_By_Default() {
        assertNavigation(BottomNavItem.Home, EmptyDestinationsNavigator)
    }

    @Test
    fun assert_Feed_Displayed_By_Navigation() {
        assertNavigation(BottomNavItem.Feed, EmptyDestinationsNavigator)
    }
}