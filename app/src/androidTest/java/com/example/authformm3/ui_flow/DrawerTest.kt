@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.ui_flow

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.R
import com.example.authformm3.ui.screens.home.HomeScreen
import com.example.authformm3.utils.Tags.TAG_DRAWER
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DrawerTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            val navController = rememberNavController()
            val navigator: DestinationsNavigator = EmptyDestinationsNavigator

            HomeScreen(navController = navController, navigator = navigator)
        }
    }

    @Test
    fun assert_Menu_Icon_Displayed() {
        val title = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.cd_menu_information)

        composeTestRule.onNodeWithContentDescription(title).assertIsDisplayed()
    }

    @Test
    fun assert_Menu_Icon_Triggers_Drawer() {
        val title = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.cd_menu_information)

        composeTestRule.onNodeWithContentDescription(title).performClick()

        composeTestRule.onNodeWithTag(TAG_DRAWER).assertIsDisplayed()
    }
}