package com.example.authformm3.ui_flow

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsSelected
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.utils.BottomBar
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class BottomBarTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            val navController = rememberNavController()

            BottomBar(navController = navController)
        }
    }

    @Test
    fun assert_Bottom_Navigation_Items_Displayed() {
        BottomNavItem.values().forEach { destination ->
            composeTestRule.onNodeWithText(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext.getString(destination.label)
            ).assertIsDisplayed()
        }
    }

    @Test
    fun assert_Current_Destination_Is_Selected() {
        val destination = BottomNavItem.Home

        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation()
                .targetContext.getString(destination.label)
        ).assertIsSelected()
    }
}