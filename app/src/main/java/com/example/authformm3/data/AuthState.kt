package com.example.authformm3.data

data class AuthState(
    val authMode: AuthMode = AuthMode.SIGN_IN,
    val email: String = "",
    val password: String = "",
    val passwordRequirements: List<PasswordRequirements> = emptyList(),
    val isLoading: Boolean = false,
    val error: String? = null
) {
    fun isFormValid() =
        password.isNotEmpty() && email.isNotEmpty()
                && authMode == AuthMode.SIGN_IN || passwordRequirements.containsAll(
            PasswordRequirements.values().toList()
        )
}