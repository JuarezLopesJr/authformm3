package com.example.authformm3.data

enum class AuthMode {
    SIGN_UP, SIGN_IN
}