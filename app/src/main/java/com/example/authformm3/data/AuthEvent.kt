package com.example.authformm3.data

sealed class AuthEvent {
    object ToggleAuthMode : AuthEvent()

    object Authenticate : AuthEvent()

    object ErrorDismissed : AuthEvent()

    class EmailChanged(val emailAddress: String) : AuthEvent()

    class PasswordChanged(val password: String) : AuthEvent()
}