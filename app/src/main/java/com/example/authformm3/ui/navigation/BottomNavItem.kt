@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.ui.navigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.authformm3.R
import com.example.authformm3.ui.screens.destinations.CalendarScreenDestination
import com.example.authformm3.ui.screens.destinations.ContactsScreenDestination
import com.example.authformm3.ui.screens.destinations.FeedScreenDestination
import com.example.authformm3.ui.screens.destinations.HomeScreenDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec

enum class BottomNavItem(
    @StringRes val label: Int,
    val route: DirectionDestinationSpec,
    val icon: ImageVector
) {
    Home(
        R.string.home_route_label,
        HomeScreenDestination,
        Icons.Default.Home
    ),

    Feed(
        R.string.feed_route_label,
        FeedScreenDestination,
        Icons.Default.List
    ),

    Contacts(
        R.string.contacts_route_label,
        ContactsScreenDestination,
        Icons.Default.Person
    ),

    Calendar(
        R.string.calendar_route_label,
        CalendarScreenDestination,
        Icons.Default.DateRange
    )
}