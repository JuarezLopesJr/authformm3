@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.ui.screens.upgrade

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.authformm3.R
import com.example.authformm3.ui.navigation.DrawerNavGraph
import com.example.authformm3.utils.ItemTopBar
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@DrawerNavGraph
@Destination
@Composable
fun UpgradeScreen(modifier: Modifier = Modifier, navigator: DestinationsNavigator) {
    Scaffold(
        modifier = modifier,
        topBar = { ItemTopBar(title = R.string.upgrade_topbar_title, navigator = navigator) },
        content = {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues = it),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Icon(
                    modifier = Modifier.size(80.dp),
                    imageVector = Icons.Default.Star,
                    contentDescription = null
                )

                Spacer(modifier = Modifier.height(16.dp))

                Text(text = stringResource(id = R.string.upgrade_topbar_title))

            }
        }
    )
}