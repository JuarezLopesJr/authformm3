@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalLifecycleComposeApi::class,
    ExperimentalLifecycleComposeApi::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class
)

package com.example.authformm3.ui.screens.auth

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.authformm3.R
import com.example.authformm3.data.AuthEvent
import com.example.authformm3.data.AuthMode
import com.example.authformm3.data.AuthState
import com.example.authformm3.data.PasswordRequirements
import com.example.authformm3.ui.screens.ErrorDialog
import com.example.authformm3.ui.screens.destinations.HomeScreenDestination
import com.example.authformm3.utils.AuthButton
import com.example.authformm3.utils.InputField
import com.example.authformm3.utils.PasswordRequirements
import com.example.authformm3.utils.Tags.TAG_CONTENT
import com.example.authformm3.utils.Tags.TAG_INPUT_EMAIL
import com.example.authformm3.utils.Tags.TAG_INPUT_PASSWORD
import com.example.authformm3.utils.Tags.TAG_PASSWORD_HIDDEN
import com.example.authformm3.utils.Tags.TAG_PROGRESS
import com.example.authformm3.utils.ToggleAuth
import com.example.authformm3.viewmodel.AuthViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@ExperimentalLifecycleComposeApi
@RootNavGraph(start = true)
@Destination
@Composable
fun AuthenticationScreen(navigator: DestinationsNavigator) {
    val authViewModel = viewModel<AuthViewModel>()
    val uiState = authViewModel.uiState.collectAsStateWithLifecycle()

    AuthenticationContent(
        authState = uiState.value,
        navigator = navigator,
        handleEvent = authViewModel::handleEvent
    )
}

@ExperimentalMaterial3Api
@Composable
fun AuthenticationContent(
    modifier: Modifier = Modifier,
    authState: AuthState,
    navigator: DestinationsNavigator,
    handleEvent: (AuthEvent) -> Unit
) {

    authState.error?.let {
        ErrorDialog(error = it, dismissError = { handleEvent(AuthEvent.ErrorDismissed) })
    }

    Box(
        modifier = modifier.fillMaxSize()
    ) {

        if (authState.isLoading) {
            CircularProgressIndicator(
                modifier = Modifier
                    .testTag(TAG_PROGRESS)
                    .align(Alignment.Center)
            )
        } else {
            AuthenticationForm(
                authMode = authState.authMode,
                email = authState.email,
                password = authState.password,
                completedRequirements = authState.passwordRequirements,
                enableAuth = authState.isFormValid(),
                onEmailChanged = {
                    handleEvent(AuthEvent.EmailChanged(it))
                },
                onPasswordChanged = {
                    handleEvent(AuthEvent.PasswordChanged(it))
                },
                onImeClicked = {
                    if (authState.isFormValid()) {
                        navigator.apply {
                            popBackStack()
                            navigate(HomeScreenDestination)
                        }
                        //handleEvent(AuthEvent.Authenticate)
                    }
                },
                onAuthClick = {
                    navigator.apply {
                        popBackStack()
                        navigate(HomeScreenDestination)
                    }
                    //handleEvent(AuthEvent.Authenticate)
                }
            )

            ToggleAuth(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 16.dp),
                authMode = authState.authMode,
                onToggleClick = { handleEvent(AuthEvent.ToggleAuthMode) })
        }
    }
}

@Composable
private fun AuthenticationForm(
    modifier: Modifier = Modifier,
    authMode: AuthMode,
    email: String,
    password: String,
    completedRequirements: List<PasswordRequirements>,
    enableAuth: Boolean,
    onEmailChanged: (String) -> Unit,
    onPasswordChanged: (String) -> Unit,
    onImeClicked: () -> Unit = {},
    onAuthClick: () -> Unit = {}
) {
    var isFieldHidden by remember { mutableStateOf(true) }
    val focusRequester = FocusRequester()

    Column(
        modifier = modifier.testTag(TAG_CONTENT),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(32.dp))
        AuthenticationTitle(authMode = authMode)
        Spacer(modifier = Modifier.height(40.dp))

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            elevation = CardDefaults.cardElevation(4.dp)
        ) {
            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                InputField(
                    modifier = Modifier
                        .testTag(TAG_INPUT_EMAIL)
                        .fillMaxWidth(),
                    value = email,
                    leadingIcon = Icons.Default.Email,
                    label = stringResource(id = R.string.label_email),
                    onValueChanged = onEmailChanged,
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Next,
                        keyboardType = KeyboardType.Email
                    ),
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusRequester.requestFocus()
                        }
                    )
                )

                Spacer(modifier = Modifier.height(24.dp))

                InputField(
                    modifier = Modifier
                        .testTag(TAG_INPUT_PASSWORD)
                        .fillMaxWidth()
                        .focusRequester(focusRequester),
                    value = password,
                    visualTransformation = if (isFieldHidden) {
                        PasswordVisualTransformation()
                    } else {
                        VisualTransformation.None
                    },
                    leadingIcon = Icons.Default.Lock,
                    label = stringResource(R.string.label_password),
                    onValueChanged = onPasswordChanged,
                    trailingIcon = {
                        Icon(
                            modifier = Modifier
                                .testTag(TAG_PASSWORD_HIDDEN + isFieldHidden)
                                .clickable(
                                    onClickLabel = if (isFieldHidden) {
                                        stringResource(id = R.string.cd_show_password)
                                    } else {
                                        stringResource(id = R.string.cd_hide_password)
                                    }
                                ) {
                                    isFieldHidden = !isFieldHidden
                                }
                                .sizeIn(48.dp),
                            imageVector = if (isFieldHidden) {
                                Icons.Default.Visibility
                            } else {
                                Icons.Default.VisibilityOff
                            },
                            contentDescription = null
                        )
                    },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Done,
                        keyboardType = KeyboardType.Password
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = { onImeClicked() }
                    )
                )
            }
        }

        Spacer(modifier = Modifier.height(8.dp))

        AnimatedVisibility(
            modifier = Modifier
                .align(Alignment.Start)
                .padding(horizontal = 28.dp),
            visible = authMode == AuthMode.SIGN_UP
        ) {
            PasswordRequirements(requirements = completedRequirements)
        }

        Spacer(modifier = Modifier.height(24.dp))

        AuthButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            authMode = authMode,
            enableAuth = enableAuth,
            onAuthClick = onAuthClick
        )
    }
}

@Composable
fun AuthenticationTitle(
    modifier: Modifier = Modifier,
    authMode: AuthMode
) {
    val title = if (authMode == AuthMode.SIGN_IN) {
        stringResource(id = R.string.label_sign_in_to_account)
    } else {
        stringResource(id = R.string.label_sign_up_for_account)
    }

    Text(
        text = title,
        modifier = modifier,
        fontSize = 24.sp,
        fontWeight = FontWeight.Black
    )
}