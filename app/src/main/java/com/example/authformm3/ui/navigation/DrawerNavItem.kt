package com.example.authformm3.ui.navigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Star
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.authformm3.R
import com.example.authformm3.ui.screens.destinations.SettingsScreenDestination
import com.example.authformm3.ui.screens.destinations.UpgradeScreenDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec

enum class DrawerNavItem(
    @StringRes val label: Int,
    val route: DirectionDestinationSpec,
    val icon: ImageVector
) {
    Upgrade(
        R.string.upgrade_route_label,
        UpgradeScreenDestination,
        Icons.Default.Star
    ),

    Settings(
        R.string.settings_route_label,
        SettingsScreenDestination,
        Icons.Default.Settings
    )
}