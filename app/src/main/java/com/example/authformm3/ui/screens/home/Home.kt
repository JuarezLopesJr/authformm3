@file:OptIn(ExperimentalLifecycleComposeApi::class)

package com.example.authformm3.ui.screens.home

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.Divider
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.authformm3.R
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.ui.navigation.DrawerNavItem
import com.example.authformm3.ui.screens.destinations.AuthenticationScreenDestination
import com.example.authformm3.ui.screens.destinations.CreationScreenDestination
import com.example.authformm3.ui.screens.destinations.HomeScreenDestination
import com.example.authformm3.utils.BottomBar
import com.example.authformm3.utils.ContentArea
import com.example.authformm3.utils.SideBar
import com.example.authformm3.utils.Tags.TAG_DRAWER
import com.example.authformm3.utils.Tags.TAG_FLOATING_BUTTON
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import java.util.Locale
import kotlinx.coroutines.launch

@ExperimentalMaterial3Api
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Destination
@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    navigator: DestinationsNavigator
) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

    val scope = rememberCoroutineScope()

    val snackbarHostState = remember { SnackbarHostState() }

    val navBackStackEntry by navController.currentBackStackEntryAsState()

    val currentDestination
            by remember(navBackStackEntry) {
                derivedStateOf {
                    navBackStackEntry?.destination?.route ?: BottomNavItem.Feed.route
                }
            }

    val snackMessage = stringResource(id = R.string.not_available_yet)

    val selectedItem =
        remember { mutableStateOf(DrawerNavItem.values()[0]) }

    val configuration = LocalConfiguration.current

    BackHandler(enabled = drawerState.isOpen) {
        scope.launch { drawerState.close() }
    }

    /* ModalNavigationDrawer MUST be the top component because M3's Scaffold
     doesn't have a drawer slot */
    ModalNavigationDrawer(
        modifier = modifier.testTag(TAG_DRAWER),
        drawerContent = {
            ModalDrawerSheet {
                Spacer(modifier = Modifier.height(12.dp))

                Text(
                    text = stringResource(R.string.drawer_title),
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(16.dp)
                )

                Divider()

                Spacer(modifier = Modifier.height(12.dp))

                DrawerNavItem.values().forEach { destination ->
                    NavigationDrawerItem(
                        icon = {
                            Icon(
                                imageVector = destination.icon,
                                contentDescription = stringResource(id = destination.label)
                            )
                        },
                        label = {
                            Text(text = stringResource(destination.label)
                                .replaceFirstChar { it.titlecase(Locale.getDefault()) })
                        },
                        selected = destination == selectedItem.value,
                        onClick = {
                            scope.launch { drawerState.close() }
                            selectedItem.value = destination
                            navigator.apply {
                                popBackStack(route = destination.route, inclusive = true)
                                navigate(destination.route)
                            }
                        },
                        modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                    )
                }

                Spacer(modifier = Modifier.weight(1f))

                Divider()

                TextButton(
                    modifier = Modifier.padding(12.dp),
                    onClick = {
                        navigator.apply {
                            popBackStack()
                            navigate(AuthenticationScreenDestination)
                        }
                    }
                ) {
                    Text(text = stringResource(id = R.string.log_out), fontSize = 16.sp)
                }
            }
        },
        drawerState = drawerState,
        content = {
            Scaffold(
                modifier = modifier,
                topBar = {
                    TopAppBar(
                        title = {
                            Text(text = stringResource(id = R.string.home_topbar_title))
                        },
                        navigationIcon = {
                            IconButton(onClick = {
                                scope.launch { drawerState.open() }
                            }) {
                                Icon(
                                    imageVector = Icons.Default.Menu,
                                    contentDescription =
                                    stringResource(id = R.string.cd_menu_information)
                                )
                            }
                        },
                        actions = {
                            if (currentDestination != BottomNavItem.Feed.route) {
                                IconButton(
                                    onClick = {
                                        scope.launch {
                                            snackbarHostState.showSnackbar(
                                                message = snackMessage
                                            )
                                        }
                                    },
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Info,
                                        contentDescription =
                                        stringResource(id = R.string.cd_more_information)
                                    )
                                }
                            }
                        }
                    )
                },
                floatingActionButton = {
                    if (configuration.orientation != Configuration.ORIENTATION_LANDSCAPE) {
                        FloatingActionButton(
                            modifier = Modifier.testTag(TAG_FLOATING_BUTTON),
                            onClick = {
                                navigator.apply {
                                    popBackStack(route = HomeScreenDestination, inclusive = false)
                                    navigate(CreationScreenDestination)
                                }
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.Add,
                                contentDescription = stringResource(id = R.string.cd_create_item)
                            )
                        }
                    }
                },
                snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
                bottomBar = {
                    if (configuration.orientation != Configuration.ORIENTATION_LANDSCAPE) {
                        BottomBar(navController = navController as NavHostController)
                    }
                },
                content = {
                    when (configuration.orientation) {
                        Configuration.ORIENTATION_LANDSCAPE -> {
                            SideBar(navController = navController)
                        }
                        else -> ContentArea(screen = BottomNavItem.Home)
                    }
                }
            )
        }
    )
}