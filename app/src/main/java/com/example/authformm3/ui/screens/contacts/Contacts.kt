@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.ui.screens.contacts

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.authformm3.R
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.utils.BottomBar
import com.example.authformm3.utils.ContentArea
import com.example.authformm3.utils.ItemTopBar
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@Destination
@Composable
fun ContactsScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    navigator: DestinationsNavigator
) {
    Scaffold(
        topBar = {
            ItemTopBar(title = R.string.contacts_topbar_title, navigator = navigator)
        },
        bottomBar = {
            BottomBar(navController = navController as NavHostController)
        },
        content = {

            ContentArea(
                screen = BottomNavItem.Contacts,
                modifier = modifier.padding(paddingValues = it)
            )
        }
    )
}