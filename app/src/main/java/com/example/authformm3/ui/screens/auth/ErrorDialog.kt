package com.example.authformm3.ui.screens

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import com.example.authformm3.R
import com.example.authformm3.utils.Tags.TAG_ERROR_ALERT
import com.example.authformm3.utils.Tags.TAG_ERROR_BUTTON

@Composable
fun ErrorDialog(
    modifier: Modifier = Modifier,
    error: String,
    dismissError: () -> Unit
) {
    AlertDialog(
        modifier = modifier.testTag(TAG_ERROR_ALERT),
        onDismissRequest = dismissError,
        confirmButton = {
            TextButton(
                onClick = dismissError,
                modifier = Modifier.testTag(TAG_ERROR_BUTTON)
            ) {
                Text(text = stringResource(id = R.string.error_action))
            }
        },
        title = {
            Text(text = stringResource(id = R.string.error_title), fontSize = 18.sp)
        },
        text = {
            Text(text = error)
        }
    )
}