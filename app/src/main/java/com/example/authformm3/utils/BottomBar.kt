@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalMaterial3Api::class,
    ExperimentalLifecycleComposeApi::class, ExperimentalLifecycleComposeApi::class,
    ExperimentalLifecycleComposeApi::class
)

package com.example.authformm3.utils

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.navigation.NavController
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.ui.screens.NavGraphs
import com.example.authformm3.ui.screens.appCurrentDestinationAsState
import com.example.authformm3.ui.screens.startAppDestination
import com.example.authformm3.utils.Tags.TAG_DESTINATION
import com.ramcosta.composedestinations.navigation.clearBackStack
import com.ramcosta.composedestinations.navigation.navigate

@Composable
fun BottomBar(
    modifier: Modifier = Modifier,
    navController: NavController
) {
    val currentDestination =
        navController.appCurrentDestinationAsState()
            .value ?: NavGraphs.root.startAppDestination

    NavigationBar(modifier = modifier) {
        BottomNavItem.values().forEach { destination ->

            NavigationBarItem(
                selected = currentDestination == destination.route,
                onClick = {
                    navController.navigate(destination.route) {
                        launchSingleTop = true
                        restoreState = true

                    }
                    navController.clearBackStack(destination.route)
                },
                icon = {
                    Icon(
                        modifier = Modifier
                            .testTag(
                                "$TAG_DESTINATION${stringResource(id = destination.label)}"
                            ),
                        imageVector = destination.icon,
                        contentDescription = stringResource(id = destination.label)
                    )
                },
                label = {
                    Text(text = stringResource(id = destination.label))
                }
            )
        }
    }
}