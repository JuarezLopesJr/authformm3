package com.example.authformm3.utils

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.utils.Tags.TAG_CONTENT_ICON
import com.example.authformm3.utils.Tags.TAG_CONTENT_TITLE

@Composable
fun ContentArea(modifier: Modifier = Modifier, screen: BottomNavItem) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .testTag(stringResource(id = screen.label)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Icon(
            modifier = Modifier
                .size(80.dp)
                .testTag(TAG_CONTENT_ICON),
            imageVector = screen.icon,
            contentDescription = stringResource(screen.label)
        )

        Spacer(modifier = Modifier.height(16.dp))

        Text(
            modifier = Modifier.testTag(TAG_CONTENT_TITLE),
            text = stringResource(id = screen.label),
            fontSize = 16.sp
        )
    }
}