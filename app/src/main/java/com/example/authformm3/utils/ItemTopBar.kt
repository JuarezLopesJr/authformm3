@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package com.example.authformm3.utils

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.authformm3.R
import com.example.authformm3.ui.screens.destinations.HomeScreenDestination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@Composable
fun ItemTopBar(
    modifier: Modifier = Modifier,
    @StringRes title: Int,
    navigator: DestinationsNavigator
) {
    TopAppBar(
        modifier = modifier,
        title = { Text(text = stringResource(id = title)) },
        navigationIcon = {
            IconButton(
                onClick = {
                    navigator.popBackStack(route = HomeScreenDestination, inclusive = false)
                }
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = stringResource(id = R.string.cd_navback_information)
                )
            }
        }
    )
}