@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.authformm3.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.clearAndSetSemantics
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.text
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.authformm3.R
import com.example.authformm3.data.AuthMode
import com.example.authformm3.data.PasswordRequirements
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATE_BUTTON
import com.example.authformm3.utils.Tags.TAG_AUTHENTICATION_TOGGLE

@Composable
private fun Requirements(
    modifier: Modifier = Modifier,
    message: String,
    isSatisfied: Boolean
) {
    val tint = if (isSatisfied) {
        MaterialTheme.colorScheme.primary
    } else {
        MaterialTheme.colorScheme.onSurface.copy(alpha = 0.4f)
    }

    /* passing description to the screen reader */
    val requirementStatus = if (isSatisfied) {
        stringResource(id = R.string.password_requirement_satisfied, message)
    } else {
        stringResource(id = R.string.password_requirement_needed, message)
    }

    Row(
        modifier = modifier
            .padding(8.dp)
            /* config for the screen reader */
            .semantics(mergeDescendants = true) {
                text = AnnotatedString(requirementStatus)
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            modifier = Modifier.size(12.dp),
            imageVector = Icons.Default.Check,
            contentDescription = null,
            tint = tint
        )

        Spacer(modifier = Modifier.width(4.dp))

        Text(
            /* to avoid description being duplicated in the screen reader */
            modifier = Modifier.clearAndSetSemantics { },
            text = message,
            fontSize = 12.sp,
            color = tint
        )
    }
}

@Composable
fun InputField(
    modifier: Modifier = Modifier,
    value: String?,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    leadingIcon: ImageVector,
    trailingIcon: @Composable () -> Unit = {},
    label: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    onValueChanged: (String) -> Unit
) {

    TextField(
        modifier = modifier,
        value = value ?: "",
        onValueChange = onValueChanged,
        label = {
            Text(text = label)
        },
        singleLine = true,
        /* visual decoration, the icon is irrelevant for the screen reader */
        leadingIcon = {
            Icon(imageVector = leadingIcon, contentDescription = null)
        },
        /* click icon, contain accessibility description */
        trailingIcon = trailingIcon,
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions
    )
}

@Composable
fun PasswordRequirements(
    modifier: Modifier = Modifier,
    requirements: List<PasswordRequirements>
) {
    Column(modifier = modifier) {
        PasswordRequirements.values().forEach {
            Requirements(
                message = stringResource(id = it.label),
                isSatisfied = requirements.containsAll(listOf(it))
            )
        }
    }
}

@Composable
fun AuthButton(
    modifier: Modifier = Modifier,
    authMode: AuthMode,
    enableAuth: Boolean,
    onAuthClick: () -> Unit
) {
    val label = if (authMode == AuthMode.SIGN_UP) {
        stringResource(id = R.string.action_sign_up)
    } else {
        stringResource(id = R.string.action_sign_in)
    }

    Button(
        modifier = modifier.testTag(TAG_AUTHENTICATE_BUTTON),
        onClick = onAuthClick,
        enabled = enableAuth
    ) {
        Text(text = label)
    }
}

@Composable
fun ToggleAuth(
    modifier: Modifier = Modifier,
    authMode: AuthMode,
    onToggleClick: () -> Unit
) {
    val label = if (authMode == AuthMode.SIGN_UP) {
        stringResource(id = R.string.action_already_have_account)
    } else {
        stringResource(id = R.string.action_need_account)
    }

    Surface(modifier = modifier, tonalElevation = 8.dp) {
        TextButton(
            onClick = onToggleClick,
            modifier = Modifier
                .testTag(TAG_AUTHENTICATION_TOGGLE)
                .background(MaterialTheme.colorScheme.surface)
                .padding(8.dp)
        ) {
            Text(text = label)
        }
    }
}

object Tags {
    const val TAG_INPUT_EMAIL = "input_email"
    const val TAG_INPUT_PASSWORD = "input_password"
    const val TAG_PASSWORD_HIDDEN = "password_hidden_"
    const val TAG_AUTHENTICATE_BUTTON = "authenticate_button"
    const val TAG_AUTHENTICATION_TOGGLE = "authentication_mode_toggle"
    const val TAG_ERROR_ALERT = "error_alert"
    const val TAG_ERROR_BUTTON = "error_button"
    const val TAG_PROGRESS = "progress"
    const val TAG_CONTENT = "content"
    const val TAG_DRAWER = "drawer"
    const val TAG_NAV_RAIL = "nav_rail"
    const val TAG_FLOATING_BUTTON = "floating_button"
    const val TAG_CONTENT_TITLE = "content_title"
    const val TAG_CONTENT_ICON = "content_icon"
    const val TAG_DESTINATION = "destination_"
}