@file:OptIn(ExperimentalLifecycleComposeApi::class, ExperimentalMaterial3Api::class)

package com.example.authformm3.utils

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationRail
import androidx.compose.material3.NavigationRailItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.navigation.NavController
import com.example.authformm3.R
import com.example.authformm3.ui.navigation.BottomNavItem
import com.example.authformm3.ui.screens.NavGraphs
import com.example.authformm3.ui.screens.appCurrentDestinationAsState
import com.example.authformm3.ui.screens.destinations.CreationScreenDestination
import com.example.authformm3.ui.screens.destinations.HomeScreenDestination
import com.example.authformm3.ui.screens.startAppDestination
import com.example.authformm3.utils.Tags.TAG_NAV_RAIL
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator.popBackStack
import com.ramcosta.composedestinations.navigation.clearBackStack
import com.ramcosta.composedestinations.navigation.navigate

@Composable
fun SideBar(
    modifier: Modifier = Modifier,
    navController: NavController
) {
    val currentDestination =
        navController.appCurrentDestinationAsState()
            .value ?: NavGraphs.root.startAppDestination

    NavigationRail(
        modifier = modifier.testTag(TAG_NAV_RAIL),
        header = {
            Spacer(Modifier.height(80.dp))

            FloatingActionButton(
                onClick = {
                    navController.apply {
                        popBackStack(route = HomeScreenDestination, inclusive = false)
                        navigate(CreationScreenDestination)
                    }
                }
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = stringResource(id = R.string.cd_create_item)
                )
            }
        }
    ) {
        BottomNavItem.values().forEach { destination ->
            NavigationRailItem(
                selected = currentDestination == destination.route,
                onClick = {
                    navController.navigate(destination.route) {
                        launchSingleTop = true
                        restoreState = true

                    }
                    navController.clearBackStack(destination.route)
                },
                icon = {
                    Icon(
                        imageVector = destination.icon,
                        contentDescription = stringResource(id = destination.label)
                    )
                },
                label = {
                    Text(text = stringResource(id = destination.label))
                }
            )
        }
    }
}