package com.example.authformm3.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.authformm3.data.AuthEvent
import com.example.authformm3.data.AuthMode
import com.example.authformm3.data.AuthState
import com.example.authformm3.data.PasswordRequirements
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AuthViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(AuthState())
    val uiState = _uiState.asStateFlow()

    private fun toggleAuthMode() {
        val authMode = uiState.value.authMode

        /* handle the event to trigger the opposite event */
        val toggledAuthMode =
            if (authMode == AuthMode.SIGN_IN) AuthMode.SIGN_UP else AuthMode.SIGN_IN

        _uiState.update {
            it.copy(authMode = toggledAuthMode)
        }
    }

    private fun updateEmail(email: String) {
        _uiState.update {
            it.copy(email = email)
        }
    }

    private fun updatePassword(password: String) {
        val requirements = mutableListOf<PasswordRequirements>()

        /* treating password validations */
        when {
            password.length > 7 -> requirements.add(PasswordRequirements.EIGHT_CHARACTERS)

            password.any { it.isUpperCase() } ->
                requirements.add(PasswordRequirements.CAPITAL_LETTER)

            password.any { it.isDigit() } -> requirements.add(PasswordRequirements.NUMBER)
        }

        _uiState.update {
            it.copy(
                password = password,
                passwordRequirements = requirements.toList()
            )
        }
    }

    private fun authenticate() {
        _uiState.update { it.copy(isLoading = true) }

        viewModelScope.launch(Dispatchers.IO) {
            /* simulating network request */
            delay(2000L)

            /* simulating UI thread emission */
            withContext(Dispatchers.Main) {
                _uiState.value = uiState.value.copy(
                    isLoading = true,
                    error = "Something went wrong"
                )
            }
        }
    }

    private fun dismissError() {
        _uiState.update { it.copy(error = null) }
    }

    fun handleEvent(authEvent: AuthEvent) {
        when (authEvent) {
            is AuthEvent.Authenticate -> authenticate()
            is AuthEvent.ToggleAuthMode -> toggleAuthMode()
            is AuthEvent.EmailChanged -> updateEmail(authEvent.emailAddress)
            is AuthEvent.PasswordChanged -> updatePassword(authEvent.password)
            is AuthEvent.ErrorDismissed -> dismissError()
        }
    }
}